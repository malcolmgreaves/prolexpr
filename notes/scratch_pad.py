def log(s):
  import time
  print "%s :: %s" % (time.asctime(time.localtime()), s)

###########################################
def getFeatVal(b):
  bits = b.split(" ")
  return (bits[0:-1], bits[-1])

def DOIT(f,w,X,feat2index):
  w = open(w,'wt')
  X = str(X)+" "
  for l in open(f):
    bits = l.strip().split("\t")
    fvs = []
    for b in bits[2:]:
      e,v = getFeatVal(b)
      fvs.append((feat2index[e],v))
    fvs.sort(key=lambda (e,v): int(e))
    w.write(X+" ".join(map(lambda (a,b): str(a)+":"+str(b), fvs))+"\n")
  w.close()

###########################################

def extractDocID(line):
  return ".".join(line.split("\t")[5].split(":")[0].split(".")[0:-1])

def reorder(inputDir,outputDir):
  import os,gzip
  files = os.listdir(inputDir)
  files.sort(key=lambda f: int(f.split("-")[1].split(".")[0]))
  rs = [gzip.open(os.path.join(inputDir,f)) for f in files]
  ws = [gzip.open(os.path.join(outputDir,f),'wt') for f in files]
  #
  n,ri,wi = len(rs),0,0
  reading = n/home/malcolm/Copy/out/kbp/rtextprocess/kbp-2012_news_corpus_all

  nLines, nDocs, nSentences = 0,0,0
  prevDocID = None
  #
  while reading > 0:
    line = rs[ri % n].readline()
    if len(line) == 0:
      # EOF
      reading -= 1
      ri += 1
    elif len(line) == 1:
      # end of sentence
      ws[wi % n].write("\n")
      nSentences += 1
      ri += 1
    else:
      nLines += 1
      # reading sentence
      try:
        docID  = extractDocID(line)
        if docID != prevDocID and prevDocID != None:
          wi += 1
          nDocs += 1
        ws[wi % n].write(line)
        prevDocID = docID
      except Exception,e:
        print "Bad line [%d]: %s : %s" % (ri,line.strip(),e)
    #
    if nLines % 1000000 == 0:
      log("wrote %d million lines in %d sentences and %d documents" % (nLines / 1000000,nSentences,nDocs))
  #
  for (r,w) in zip(rs,ws):
    r.close()
    w.close()
  #
  return nLines,nSentences,nDocs

def X():
  import os
  inputDir = "/home/malcolm/local/out/kbp/rtextprocess/kbp-2012_news_corpus_all"
  outputDir = "/home/malcolm/local/out/kbp/rtextprocess/reordered-kbp-2012_news_corpus_all"
  log("reordering these [%d] files: %s" % (len(os.listdir(inputDir)),inputDir))
  log("writing them to here:        %s" % outputDir)
  nLines,nSentences,nDocs = reorder(inputDir,outputDir)
  log("done, wrote %d lines in %d sentences in %d documents" % (nLines,nSentences,nDocs))


def test():
  import os,gzip
  r = gzip.open(os.path.join("/home/malcolm/Copy/out/kbp/rtextprocess/kbp-2012_news_corpus_all",os.listdir("/home/malcolm/Copy/out/kbp/rtextprocess/kbp-2012_news_corpus_all")[0]))
  print extractDocID(r.readline())
  r.close()

test()